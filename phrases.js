export const phrases = {
    greetings:
        'Hi! My name is AltF4 and today I will be your personal online commentator!<br> Now the game is about to start, so prepare and remember - it`s all about 2 M`s - moment and positioning.',
    rating_start: 'And here we start! Staring rating list: ',
    sec30: '30 seconds came to the end. Let`s see current players` results! Rating list:<br>',
    nearly_finished_thirty:
        'Wow-wow-wow! Some crazy guy has only 30 symbols left! It is ',
    nearly_finished_ten:
        'We do nearly have a winner!!! 10 symbols left, hurry up! We are ready to congratulate ',
    joke1: `Well, the game is pretty boring. Let me tell you a joke.<br>
    <strong>A man in talking to God</strong> The man: "God, how long is a million years?"<br>God:"To me it's a about a minute"<br>--"God, how much is a millian dollars?"<br>--"TO me it's a penny"<br>--"God, may I have a penny?"<br><strong>"Wait a minute..."</strong>
    `,
    joke2: 'Two fish in a tank. One says: “How do you drive this thing?',
    joke3: 'Two guys walk into a bar, and one of them orders a mushroom soup. The other guy asks him how the mushroom soup tastes, to which he responds "It`s good, but there is mushroom for improvement"',
    story1: 'Well you know, actually I`m happy. Yeah, my son is a very successful guy. He works as a team lead in Apple, yes yes you of course can believe me',
    story2: '*starts singing* Feel invincible, earthquake poverfull, just like a tidal wave, you make me brake, you`re my titanium, fight song raising up, like a roar of victory in the stadium',
    end_game: `The game is ended. Here are the winners!`,
};
