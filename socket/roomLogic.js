export const rooms = new Map();
export const roomIds = [];
export const usernamesInRooms = {};
export const userStatuses = new Map();
export let roomsStarted = [];
export const roomTexts = new Map();
export const userNumGames = new Map();
import * as config from './config';
import { getText } from './commentator';
export const createRoom = (socket, io, data) => {
    if (!roomIds.includes(data.name)) {
        roomIds.push(data.name);
        rooms.set(data.name, 0);
        io.emit(
            'CREATE_ROOM',
            Object.assign(data, { users: rooms.get(data.name) }),
        );
        joinRoom(socket, io, data.name);
    } else {
        io.to(socket.id).emit(
            'CREATE_ROOM_ERROR',
            'This name of room is taken',
        );
    }
};

export const joinRoom = (socket, io, roomId) => {
    socket.join(roomId, () => {
        const playersInRoom = rooms.get(roomId);
        rooms.set(roomId, playersInRoom + 1);
        if (usernamesInRooms.hasOwnProperty(roomId)) {
            usernamesInRooms[roomId].push(socket.handshake.query.username);
        } else {
            usernamesInRooms[roomId] = [socket.handshake.query.username];
        }
        //set status 'not ready'
        userStatuses.set(socket.handshake.query.username, 0);
        io.to(socket.id).emit('UPDATE_ROOM_USER', {
            statusesArr: Array.from(userStatuses),
        });
        const correctRooms = Array.from(rooms).filter(
            (item) =>
                item[1] < config.MAXIMUM_USERS_FOR_ONE_ROOM &&
                !roomsStarted.includes(item[0]),
        );
        io.emit('UPDATE_ROOMS', correctRooms);
        io.in(roomId).emit('JOIN_ROOM', {
            roomId: roomId,
            usersInRoom: usernamesInRooms[roomId],
            statusesArr: Array.from(userStatuses),
        });
    });
};

export const leaveRoom = (socket, io, roomId) => {
    socket.leave(roomId, () => {
        const playersInRoom = rooms.get(roomId);
        rooms.set(roomId, playersInRoom - 1);
        const tempIndex = usernamesInRooms[roomId].indexOf(
            socket.handshake.query.username,
        );
        usernamesInRooms[roomId].splice(tempIndex, 1);
        io.to(socket.id).emit('LEAVE_ROOM_USER');
        io.to(roomId).emit('LEAVE_ROOM', {
            roomId: roomId,
            usersInRoom: usernamesInRooms[roomId],
            statusesArr: Array.from(userStatuses),
        });
        if (usernamesInRooms[roomId].length == 0) {
        }
        const correctRooms = Array.from(rooms).filter(
            (item) =>
                item[1] < config.MAXIMUM_USERS_FOR_ONE_ROOM &&
                !roomsStarted.includes(item[0]),
        );
        io.emit('UPDATE_ROOMS', correctRooms);
        if (roomsStarted.includes(roomId)) {
            if (
                finishedUsers[roomId].includes(socket.handshake.query.username)
            ) {
                const indexOfFinished = finishedUsers[roomId].indexOf(
                    socket.handshake.query.username,
                );
                finishedUsers[roomId].splice(indexOfFinished, 1);
            }
            if (finishedUsers[roomId].length == 0) {
                roomsStarted = roomsStarted.filter((room) => room != roomId);
            }
        }
        if (checkIfAllReadyInRoom({ room: roomId })) {
            io.to(roomId).emit('CREATE_COMMENTATOR', 'greetings');
            startTimer(socket, io, roomId);
        }
    });
};

export const changeStatus = (socket, io, data) => {
    if (userStatuses.get(data.name)) {
        userStatuses.set(data.name, 0);
    } else userStatuses.set(data.name, 1);

    io.to(data.room).emit('UPDATE_ROOM', {
        roomId: data.room,
        roomUsers: usernamesInRooms[data.room],
        statusesArr: Array.from(userStatuses),
    });
    io.to(socket.id).emit('UPDATE_ROOM_USER', {
        statusesArr: Array.from(userStatuses),
    });
    if (checkIfAllReadyInRoom({ room: data.room })) {
        if (!roomsStarted.includes(data.room)) {
            io.to(data.room).emit('CREATE_COMMENTATOR', 'greetings');

            startTimer(socket, io, data.room);
        }
    }
};

const checkIfAllReadyInRoom = (data) => {
    const usersInRoom = usernamesInRooms[data.room];
    for (let i = 0; i < usersInRoom.length; i++) {
        if (userStatuses.get(usersInRoom[i]) == 0) {
            return false;
        }
    }
    return true;
};

//config.SECONDS_TIMER_BEFORE_START_GAME
const startTimer = (socket, io, roomId) => {
    roomsStarted.push(roomId);
    const seconds = config.SECONDS_TIMER_BEFORE_START_GAME;
    finishedUsers[roomId] = [];
    io.to(roomId).emit('START_TIMER', seconds);
};

export const setRoomText = (socket, io, data) => {
    roomTexts.set(data.roomName, data.text);
};

export const progressAdd = (socket, io, data) => {
    const percentWidth = (data.typed * 100) / data.length;
    if (percentWidth == 100) {
        if (!finishedUsers[data.roomId].includes(data.username)) {
            finishedUsers[data.roomId].push(data.username);
        }
    }
    if (data.length - data.typed == 30) {
        io.to(data.roomId).emit('nearly_finish_thirty', data.username);
    }
    if (data.length - data.typed == 10) {
        io.to(data.roomId).emit('nearly_finish_ten', data.username);
    }
    io.to(data.roomId).emit('PROGRESS_ADD', {
        name: data.username,
        width: percentWidth,
    });
    if (checkAllFinished(socket, io, data.roomId)) {
        io.to(data.roomId).emit('END_GAME');
    }
};

const finishedUsers = {};

const checkAllFinished = (socket, io, room) => {
    for (let user of usernamesInRooms[room]) {
        if (!finishedUsers[room].includes(user)) {
            return false;
        }
    }
    return true;
};

export const getResultTable = (socket, io, data) => {
    const resultTable = [];
    roomsStarted.splice(roomsStarted.indexOf(data.roomName), 1);
    roomTexts.delete(data.roomName);
    for (let i of finishedUsers[data.roomName]) {
        resultTable.push(i);
    }
    for (let i = 0; i < data.undoneUsers.length; i++) {
        resultTable.push(data.undoneUsers[i].name);
    }
    io.to(data.roomName).emit('GET_RESULT_TABLE', resultTable);
    io.to(data.roomName).emit('END_GAME_COMMENTATOR', resultTable);
};

export const deleteRoom = (socket, io, name) => {
    rooms.delete(name);
    const indexRoom = roomIds.indexOf(name);
    roomIds.splice(indexRoom, 1);
    delete usernamesInRooms[name];
    const indexRoomStarted = roomsStarted.indexOf(name);
    roomsStarted.splice(indexRoomStarted, 1);
    roomTexts.delete(name);
};
