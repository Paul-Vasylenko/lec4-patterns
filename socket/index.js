import * as config from './config';
import { texts } from '../data';
import {
    createRoom,
    rooms,
    joinRoom,
    usernamesInRooms,
    leaveRoom,
    changeStatus,
    roomsStarted,
    roomIds,
    setRoomText,
    roomTexts,
    progressAdd,
    deleteRoom,
    getResultTable,
    userStatuses,
    userNumGames,
} from './roomLogic.js';
import { checkUser, deleteUserName } from './userLogic.js';
import {
    getText,
    startRatingCommentator,
    currentThirtySec,
    nearlyFinishThirty,
    nearlyFinishTen,
    endGameCommentator,
    sayJoke,
} from './commentator.js';
import _ from 'lodash';

export default (io) => {
    io.on('connection', (socket) => {
        //download rooms that are created already
        const correctRooms = Array.from(rooms).filter((item) => {
            if (
                item[1] < config.MAXIMUM_USERS_FOR_ONE_ROOM &&
                !roomsStarted.includes(item[0])
            )
                return item;
        });
        io.to(socket.id).emit('UPDATE_ROOMS', correctRooms);

        //login
        socket.on('CHECK_USER', (inputValue) => {
            checkUser(socket, io, inputValue);
        });
        //on creation add new room to database
        socket.on('CREATE_ROOM', (data) => {
            createRoom(socket, io, data);
        });
        //Join room btn handler
        socket.on('JOIN_ROOM', (data) => {
            joinRoom(socket, io, data);
        });
        socket.on('LEAVE_ROOM', (data) => {
            leaveRoom(socket, io, data);
        });
        socket.on('DELETE_ROOM', (name) => {
            deleteRoom(socket, io, name);
        });
        socket.on('CHANGE_STATUS', (data) => {
            changeStatus(socket, io, data);
            if (roomsStarted.includes(data.room)) {
                const correctRooms = roomIds.filter(
                    (item) => !roomsStarted.includes(item),
                );

                io.emit('UPDATE_ROOMS', correctRooms);
            }
        });
        socket.on('CREATE_TEXT', (roomName) => {
            const id = getRandomInt(texts.length - 1);
            io.to(roomName).emit('CREATE_TEXT', id);
        });
        socket.on('SET_ROOM_TEXT', (data) => {
            setRoomText(socket, io, data);
        });
        socket.on('SPAWN_TEXT', (roomName) => {
            io.to(roomName).emit('SPAWN_TEXT', roomTexts.get(roomName));
        });
        socket.on('PROGRESS_ADD', (data) => {
            progressAdd(socket, io, data);
        });
        socket.on('SPAWN_GAME_TIMER', (roomId) => {
            io.to(roomId).emit('SPAWN_GAME_TIMER', config.SECONDS_FOR_GAME);
        });
        socket.on('GET_RESULT_TABLE', (data) => {
            getResultTable(socket, io, data);
        });
        socket.on('SET_STATUSES_UNREADY', (roomId) => {
            for (let user of usernamesInRooms[roomId]) {
                if (!userNumGames.has(user)) {
                    userNumGames.set(user, 1);
                } else {
                    userNumGames.set(user, userNumGames.get(user) + 1);
                }
                userStatuses.set(user, 0);
            }
        });
        socket.on('rating_start_commentator', (roomName) => {
            _.curry(startRatingCommentator)(socket)(io)(roomName);
        });
        socket.on('current_thirty_sec', (data) => {
            _.curry(currentThirtySec)(socket)(io)(data);
        });
        socket.on('RELOAD_ROOM', (roomId) => {
            io.to(roomId).emit('UPDATE_ROOM', {
                roomId: roomId,
                roomUsers: usernamesInRooms[roomId],
                statusesArr: Array.from(userStatuses),
            });
            io.to(socket.id).emit('UPDATE_ROOM_USER', {
                statusesArr: Array.from(userStatuses),
            });
        });
        socket.on('nearly_finish_thirty', (data) => {
            _.curry(nearlyFinishThirty)(socket)(io)(data);
        });
        socket.on('nearly_finish_ten', (data) => {
            _.curry(nearlyFinishTen)(socket)(io)(data);
        });
        socket.on('RELOAD_ROOM_USER', (roomId) => {
            io.to(socket.id).emit('UPDATE_ROOM', {
                roomId: roomId,
                roomUsers: usernamesInRooms[roomId],
                statusesArr: Array.from(userStatuses),
            });
            io.to(socket.id).emit('UPDATE_ROOM_USER', {
                statusesArr: Array.from(userStatuses),
            });
            io.to(roomId).emit('DELETE_COMMENTATOR');
        });
        socket.on('get_text', (option) => {
            _.curry(getText)(socket)(io)(option);
        });
        socket.on('END_GAME_COMMENTATOR', (data) => {
            _.curry(endGameCommentator)(socket)(io)(data);
        });
        socket.on('SAY_JOKE', (roomId) => {
            _.curry(sayJoke)(socket)(io)(roomId);
        });
        socket.on('disconnect', () => {
            const userInRoom = checkIfUserInRoom(socket);
            if (userInRoom) {
                leaveRoom(socket, io, userInRoom);
            }
            deleteUserName(socket.handshake.query.username);
        });
    });
};

const checkIfUserInRoom = (socket) => {
    const keys = Object.keys(usernamesInRooms);
    for (let key of keys) {
        if (usernamesInRooms[key].includes(socket.handshake.query.username)) {
            return key;
        }
    }
    return false;
};

export const getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
};
