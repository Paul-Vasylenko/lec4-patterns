const usernames = new Set();

export const checkUser = (socket, io, inputValue) => {
    if (usernames.has(inputValue)) {
        io.emit('CHECK_USER_ERROR', inputValue);
    } else {
        usernames.add(inputValue);
        io.emit('CHECK_USER_SUCCESS', inputValue);
    }
};

export const deleteUserName = (name) => {
    usernames.delete(name);
};
