import { phrases } from '../phrases.js';
import { usernamesInRooms, userNumGames } from './roomLogic.js';
import { getRandomInt } from './index.js';
const roomPhrases = new Map();
const transportRepository = [
    'Traktor',
    'Tesla',
    'Horse',
    'Player#2',
    'On foot',
];
export const getText = (socket, io, data) => {
    //data has option and room
    let factory = new FactoryMessage();
    const message = factory.create(data);
    //console.log(message);
    roomPhrases.set(data.room, message.message);
    io.to(data.room).emit('CHANGE_PHRASE', message);
};

//Factory design pattern
class FactoryMessage {
    create(data) {
        let message;
        switch (data.option) {
            case 'greetings':
                message = new GreetingsMsg();
                break;

            case 'rating_start':
                message = new RatingStartMsg(data);
                break;
            case 'sec30':
                message = new RatingCurrent(data);
                break;
            case 'nearly_finished_thirty':
                message = new NearlyFinishedThirty(data);
                break;
            case 'nearly_finished_ten':
                message = new NearlyFinishedTen(data);
                break;
            case 'end_game':
                message = new EndGameMsg(data);
                break;
            case 'joke1':
            case 'joke2':
            case 'joke3':
            case 'story1':
            case 'story2':
                message = new JokeMsg(data);
                break;
        }
        return message;
    }
}

class GreetingsMsg {
    constructor() {
        this.option = 'greetings';
        this.message = phrases[this.option];
    }
}

class RatingStartMsg {
    constructor(data) {
        this.option = 'rating_start';
        this.message = phrases[this.option];
        let i = 1;
        for (let user of data.usersInRoom) {
            const numOfGames = userNumGames.get(user);
            this.message += `${i}) ${user}(${
                numOfGames ? numOfGames : 0
            }), riding on ${transportRepository[-1 + i++]}; `;
        }
    }
}
class RatingCurrent {
    constructor(data) {
        this.option = 'sec30';
        this.message = phrases[this.option];
        let i = 1;
        for (let user of data.usersInRoom) {
            const numOfGames = userNumGames.get(user);
            this.message += `${i++}) ${user.name}(${
                numOfGames ? numOfGames : 0
            }); `;
        }
        if (data.diff > 0) {
            this.message += `; Difference between players 1 and 2 is ${data.diff}%`;
        }
        this.message += `; Time left: ${data.time}`;
    }
}
class NearlyFinishedThirty {
    constructor(data) {
        this.option = 'nearly_finished_thirty';
        this.message = phrases[this.option] + data.userName;
    }
}
class NearlyFinishedTen {
    constructor(data) {
        this.option = 'nearly_finished_ten';
        this.message = phrases[this.option] + data.userName + ' with his win!';
    }
}
class EndGameMsg {
    constructor(data) {
        this.option = 'end_game';
        this.message = phrases[this.option];
        for (let i = 0; i < data.users.length; i++) {
            if (data.users[i]) {
                this.message += `${i + 1})${data.users[i]}`;
            }
        }
    }
}
const cache = new Map();
class JokeMsg {
    constructor(data) {
        /*
        Using proxy pattern. It would be great if we used, for example, fetch,
        in this way we could prevent creating some of server requests, that
        will increase app speed.
        */
        this.option = data.option;
        const proxiedMsg = new Proxy(createMessage, {
            apply(target, thisArg, args) {
                const option = args[0];
                let message;
                if (cache.has(option)) {
                    message = cache.get(option);
                } else {
                    cache.set(option, createMessage(option));
                    message = cache.get(option);
                }
                return message;
            },
        });
        this.message = proxiedMsg(this.option);
    }
}

function createMessage(option) {
    return 'I`m bored. Let me tell you smth: ' + phrases[option];
}
export const startRatingCommentator = (socket, io, roomName) => {
    const usersInRoom = usernamesInRooms[roomName];
    getText(socket, io, {
        option: 'rating_start',
        room: roomName,
        usersInRoom,
    });
};
export const currentThirtySec = (socket, io, data) => {
    getText(socket, io, {
        option: 'sec30',
        room: data.roomName,
        usersInRoom: data.ratingusers,
        diff: data.diffOneTwoUser,
        time: data.seconds,
    });
};
export const nearlyFinishThirty = (socket, io, data) => {
    getText(socket, io, {
        option: 'nearly_finished_thirty',
        room: data.roomName,
        userName: data.name,
    });
};

export const nearlyFinishTen = (socket, io, data) => {
    getText(socket, io, {
        option: 'nearly_finished_ten',
        room: data.roomName,
        userName: data.name,
    });
};

export const endGameCommentator = (socket, io, data) => {
    getText(socket, io, {
        option: 'end_game',
        room: data.roomName,
        users: data.firstThree,
    });
};

export const sayJoke = (socket, io, data) => {
    const options = ['joke1', 'joke2', 'joke3', 'story1', 'story2'];
    const num = getRandomInt(options.length);
    const option = options[num];

    getText(socket, io, {
        option: option,
        room: data.roomName,
    });
};
