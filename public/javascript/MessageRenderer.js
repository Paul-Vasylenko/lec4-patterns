import { createElement } from './helpers/helper.mjs';

export class MessageRenderer {
    constructor() {
        this.timeout = null;
        this.secondsNotTalking = 0;
        this.intervalNotTalking = null;
    }
    addMessage(message) {
        if (this.checkForMessage()) {
            this.deleteMessage();
        }
        //FACADE use.
        //It is better to use when we have more classes to deal
        //But still it is a good example
        const appender = new MessageAppender();
        appender.append(message);
        this.timeout = setTimeout(() => {
            if (message.substr(0, 8) != 'The game') {
                this.deleteMessage();
            }
        }, 5000);
        this.secondsNotTalking = 0;
    }
    checkForMessage() {
        const exists = document.querySelector('.words-text');
        return exists ? true : false;
    }
    deleteMessage() {
        const deleter = new MessageDeleter();
        deleter.delete();
        clearTimeout(this.timeout);
        if (this.intervalNotTalking) {
            clearInterval(this.intervalNotTalking);
            this.secondsNotTalking = 0;
        }
        this.intervalNotTalking = setInterval(() => {
            this.secondsNotTalking++;
            if (this.secondsNotTalking == 8) {
                this.secondsNotTalking = 0;
            }
        }, 1000);
    }
}

class MessageAppender {
    append(message) {
        const block = document.querySelector('.words');
        const p = createElement({
            tagName: 'p',
            className: 'words-text',
        });
        p.innerHTML = message;
        block.append(p);
    }
}

class MessageDeleter {
    delete() {
        const messageBlock = document.querySelector('.words-text');
        messageBlock?.remove();
    }
}
