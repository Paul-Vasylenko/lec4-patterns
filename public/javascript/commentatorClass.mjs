import { removeClass, addClass } from './helpers/helper.mjs';
import { MessageRenderer } from './MessageRenderer.js';
const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

export const socket = io('', { query: { username } });

//Facade
/* */
class Commentator extends MessageRenderer {
    constructor() {
        super();
    }
    create(data) {
        const commBlock = document.querySelector('.commentator-area');
        //using currying lodash
        _.curry(removeClass)(commBlock)('display-none');
        socket.emit('get_text', data);
    }
    delete() {
        const commBlock = document.querySelector('.commentator-area');
        const commBlockAddClass = _.curry(addClass)(commBlock);
        commBlockAddClass('display-none');
        clearInterval(this.interval);
    }
}

export let commentator = new Commentator();
