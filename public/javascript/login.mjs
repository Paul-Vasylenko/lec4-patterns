import { showMessage } from './helpers/DOMRender.mjs';
const username = sessionStorage.getItem('username');

if (username) {
    window.location.replace('/game');
}
const socket = io('');

const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
    const inputValue = getInputValue();
    if (!inputValue) {
        return;
    }
    socket.emit('CHECK_USER', inputValue);
};

const onKeyUp = (ev) => {
    const enterKeyCode = 13;
    if (ev.keyCode === enterKeyCode) {
        submitButton.click();
    }
};

socket.on('CHECK_USER_ERROR', (inputValue) => {
    const modalDiv = showMessage(inputValue + ' is taken');
    document.body.append(modalDiv);
    sessionStorage.removeItem('username');
});
socket.on('CHECK_USER_SUCCESS', (inputValue) => {
    sessionStorage.setItem('username', inputValue);
    window.location.replace('/game');
});

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
