import { getRoomBlock, renderPlayerBar } from '../helpers/DOMRender.mjs';
import { addClass, removeClass } from '../helpers/helper.mjs';
import { socket } from '../game.mjs';
const container = document.querySelector('.availible-rooms');

export const createRoom = (data) => {
    const roomLi = getRoomBlock(data);
    const roomJoinBtn = roomLi.querySelector('.join-btn');
    roomJoinBtn.addEventListener('click', () => {
        socket.emit('JOIN_ROOM', data.name);
    });
    container.append(roomLi);
};

export const updateRooms = (rooms) => {
    container.innerHTML = '';
    if (rooms.length > 0) {
        rooms.forEach((item) => {
            const data = { name: item[0], users: item[1] };
            if (data.users > 0) {
                const roomLi = getRoomBlock(data);
                const roomJoinBtn = roomLi.querySelector('.join-btn');
                roomJoinBtn.addEventListener('click', () => {
                    socket.emit('JOIN_ROOM', data.name);
                });
                if (data.users != 5) container.append(roomLi);
            } else {
                socket.emit('DELETE_ROOM', data.name);
            }
        });
    }
};

export const joinRoom = (data) => {
    addClass(document.querySelector('#rooms-page'), 'display-none');
    removeClass(document.querySelector('#game-page'), 'display-none');
    const ul = document.querySelector('.user-list-ul');
    ul.innerHTML = '';
    const statuses = new Map(data.statusesArr);
    for (let i = 0; i < data.usersInRoom.length; i++) {
        const currentUser = data.usersInRoom[i];
        renderPlayerBar(
            currentUser,
            i + 1,
            statuses.get(currentUser),
            data.roomId,
        );
    }
};
