import {
    renderPlayerBar,
    createTimerBlock,
    showMessage,
} from '../helpers/DOMRender.mjs';
import { socket } from '../game.mjs';
import { createElement, addClass, removeClass } from '../helpers/helper.mjs';
import { commentator } from '../commentatorClass.mjs';
const readyBtn = document.querySelector('#readyBtn');

export const leaveRoomUser = () => {
    removeClass(document.querySelector('#rooms-page'), 'display-none');
    addClass(document.querySelector('#game-page'), 'display-none');
};

export const leaveRoom = (data) => {
    const ul = document.querySelector('.user-list-ul');
    ul.innerHTML = '';
    const statuses = new Map(data.statusesArr);
    for (let i = 0; i < data.usersInRoom.length; i++) {
        const currentUser = data.usersInRoom[i];
        renderPlayerBar(
            currentUser,
            i + 1,
            statuses.get(currentUser),
            data.roomId,
        );
    }
    if (data.usersInRoom.length == 0) {
        socket.emit('DELETE_ROOM', data.roomId);
    }
};

export const updateRoom = (data) => {
    const ul = document.querySelector('.user-list-ul');
    ul.innerHTML = '';
    for (let i = 0; i < data.roomUsers?.length; i++) {
        const currentuser = data.roomUsers[i];
        let userStatus = data.statusesArr.filter(
            (item) => item[0] == currentuser,
        );
        renderPlayerBar(currentuser, i + 1, userStatus[0][1]);
    }
};

export const updateRoomUser = (data) => {
    const currentuser = sessionStorage.getItem('username');
    let userStatus = data.statusesArr.filter((item) => item[0] == currentuser);
    if (userStatus[0][1] == 1) {
        readyBtn.innerText = 'Not ready';
    } else {
        readyBtn.innerText = 'Ready';
    }
};

export const startTimer = (seconds) => {
    createTimerBlock(seconds);
    document.querySelector('#quit-room-btn').style.display = 'none';
    const roomName = document.querySelector('.room-name').innerText;

    socket.emit('CREATE_TEXT', roomName);
    const timerP = document.querySelector('.timer');
    timerP.innerText = seconds;
    const timer = setInterval(() => {
        timerP.innerText = --seconds;
        if (seconds == 0) {
            clearInterval(timer);
            startGame();
        }
    }, 1000);
};

export const startGame = () => {
    document.querySelector('.timer').remove();
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('rating_start_commentator', roomName);
    socket.emit('SPAWN_TEXT', roomName);
    window.addEventListener('keyup', keyHandler);
    socket.emit('SPAWN_GAME_TIMER', roomName);
};
//proxy
const cache = new Map();
export async function getTextById(id) {
    const roomName = document.querySelector('.room-name').innerText;
    let resultText;
    if (cache.has(id)) {
        resultText = cache.get(id);
    } else {
        resultText = await fetch('http://localhost:3002/game/texts/' + id);
        cache.set(id, resultText);
    }
    const resultJson = await resultText.json();
    const text = resultJson.result;
    socket.emit('SET_ROOM_TEXT', { text, roomName });
}
export const spawnText = (text) => {
    const area = document.querySelector('.game-area');
    let checker = document.querySelector('.game-text');
    if (!checker) {
        const p = createElement({
            tagName: 'p',
            className: 'game-text',
            attributes: {
                id: 'text-container',
            },
        });
        p.innerHTML = `<span class="typed"></span><span class="next">${
            text[0]
        }</span><span class='text-left'>${text.substr(1)}</span>`;
        area.append(p);
    }
};

export const keyHandler = (e) => {
    const pressed = e.key;
    const nextChar = document.querySelector('.next').innerText;
    const typed = document.querySelector('.typed');
    const next = document.querySelector('.next');
    const left = document.querySelector('.text-left');
    if (pressed.codePointAt() >= 32 && pressed.codePointAt() <= 126) {
        if (pressed == nextChar) {
            typed.textContent += nextChar;
            next.textContent = left.textContent[0];
            left.textContent = left.textContent.substr(1);
        }
    }
    const roomName = document.querySelector('.room-name').innerText;
    let fulllength = typed.textContent.length + left.textContent.length;
    if (left.textContent.length > 0 || next.textContent.length == 1)
        fulllength++;
    socket.emit('PROGRESS_ADD', {
        username: sessionStorage.getItem('username'),
        roomId: roomName,
        length: fulllength,
        typed: typed.innerText.length,
    });
};

function sortUndoneUsers(percent = '0%') {
    const bars = document.getElementsByClassName('user-progress');
    const undoneUsers = [];
    for (let i of bars) {
        if (!i.style.width) i.style.width = '0%';
        if (percent == '0%' || i.style.width != percent) {
            undoneUsers.push({
                name: i.classList[1],
                width: i.style.width ? i.style.width : '0%',
            });
        }
    }
    undoneUsers.sort(function (a, b) {
        if (parseInt(a.width) > parseInt(b.width)) {
            return -1;
        } else if (parseInt(a.width) == parseInt(b.width)) {
            return 0;
        } else {
            return 1;
        }
    });
    return undoneUsers;
}

export const endGame = () => {
    window.removeEventListener('keyup', keyHandler);
    const undoneUsers = sortUndoneUsers('100%');

    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('SET_STATUSES_UNREADY', roomName);
    socket.emit('GET_RESULT_TABLE', { undoneUsers, roomName });
};

export const progressAdd = (data) => {
    const progressBar = document.querySelector(`.user-progress.${data.name}`);
    if (progressBar) {
        progressBar.style.width = data.width + '%';
        if (data.width == 100) {
            addClass(progressBar, 'finished');
        }
    }
};
let timer;
export const spanwGameTimer = (seconds) => {
    let commentatorcounter = 0;
    const roomName = document.querySelector('.room-name').innerText;
    const area = document.querySelector('.rightside');
    const checker = document.querySelector('.game-timer');
    if (!checker) {
        const timerblock = createElement({
            tagName: 'div',
            className: 'game-timer',
        });
        timerblock.innerHTML = `<span class='seconds-left'>${seconds}</span> second left`;
        area.append(timerblock);
        const timerSpan = document.querySelector('.seconds-left');
        timerSpan.innerText = seconds;
        timer = setInterval(() => {
            commentator.interval = timer;
            timerSpan.innerText = --seconds;
            commentatorcounter++;
            if (commentatorcounter % 30 == 0) {
                //30 seconds
                const ratingusers = sortUndoneUsers();
                const user1 = ratingusers[0].name;
                let diffOneTwoUser = 0;
                const user2 = ratingusers[1]?.name;
                if (user2) {
                    const user1Width = document.querySelector(
                        '.user-progress.' + user1,
                    ).style.width;
                    const user2Width = document.querySelector(
                        '.user-progress.' + user2,
                    ).style.width;
                    diffOneTwoUser =
                        parseFloat(user1Width) - parseFloat(user2Width);
                }

                socket.emit('current_thirty_sec', {
                    roomName,
                    ratingusers,
                    diffOneTwoUser,
                    seconds,
                });
            }
            if (commentator.secondsNotTalking == 7) {
                socket.emit('SAY_JOKE', { roomName });
            }
            if (seconds == 0) {
                clearInterval(timer);
                const finishedAlreadyModal = document.querySelector('.modal');
                const finishedAlreadyStatus =
                    document.querySelector('.ready-status-red');
                if (!finishedAlreadyModal && !finishedAlreadyStatus) {
                    endGame();
                }
            }
        }, 1000);
    }
};

export const getResultTable = (resultTable) => {
    let resultstr = ``;
    let k = 1;
    clearInterval(timer);
    for (let i of resultTable) {
        resultstr += `${k}) <span id="place-${k++}">${i}</span><br>`;
    }
    const checkerModalExist = document.querySelector('.modal');
    if (!checkerModalExist) {
        showMessage(resultstr);
    }
    const roomName = document.querySelector('.room-name').innerText;

    document.querySelector('.modal-closebtn').addEventListener('click', () => {
        socket.emit('RELOAD_ROOM_USER', roomName);
        document.querySelector('#quit-room-btn').style.display = 'block';
        document.querySelector('#readyBtn').style.display = 'block';
        document.querySelector('.modal')?.remove();
        document.querySelector('.game-text')?.remove();
        document.querySelector('.game-timer')?.remove();
    });
    document
        .querySelector('.modal-closebtn')
        .setAttribute('id', 'quit-results-btn');
};

export const reloadRoom = () => {
    document.querySelector('#quit-room-btn').style.display = 'block';
    document.querySelector('#readyBtn').style.display = 'block';
    document.querySelector('.modal')?.remove();
    document.querySelector('.game-text')?.remove();
};
