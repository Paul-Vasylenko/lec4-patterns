import { commentator } from '../commentatorClass.mjs';

export const createCommentator = (option) => {
    const roomName = document.querySelector('.room-name').innerText;
    commentator.create({
        option: option,
        room: roomName,
    });
};

export const changePhrase = ({ message }) => {
    console.log(message);
    commentator.addMessage(message);
};

export const deleteCommentator = () => {
    commentator.delete();
};
