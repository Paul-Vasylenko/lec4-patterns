export function addAllEventListeners(socket) {
    const container = document.querySelector('.availible-rooms');
    const createBtn = document.querySelector('#add-room-btn');
    const leaveRoomBtn = document.querySelector('#quit-room-btn');
    const readyBtn = document.querySelector('#readyBtn');

    window.addEventListener('load', () => {
        socket.emit('CHECK_USER', sessionStorage.getItem('username'));
    });

    readyBtn.addEventListener('click', () => {
        const roomId = document.querySelector('.room-name').innerText;
        socket.emit('CHANGE_STATUS', {
            room: roomId,
            name: sessionStorage.getItem('username'),
        });
    });
    createBtn.addEventListener('click', () => {
        const roomName = prompt('Enter room name: ');
        if (roomName) {
            socket.emit('CREATE_ROOM', { name: roomName });
        }
    });
    leaveRoomBtn.addEventListener('click', () => {
        const roomName = document.querySelector('.room-name').innerText;
        socket.emit('LEAVE_ROOM', roomName);
    });
}
