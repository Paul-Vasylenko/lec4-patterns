import { showMessage } from './helpers/DOMRender.mjs';
import { addAllEventListeners } from './functions/eventListeners.mjs';
import { createRoom, updateRooms, joinRoom } from './functions/roompage.mjs';
import {
    leaveRoomUser,
    leaveRoom,
    updateRoom,
    updateRoomUser,
    startTimer,
    getTextById,
    spawnText,
    endGame,
    progressAdd,
    spanwGameTimer,
    getResultTable,
    reloadRoom,
} from './functions/gamepage.mjs';
import {
    createCommentator,
    changePhrase,
    deleteCommentator,
} from './functions/commentator.mjs';
import { commentator } from './commentatorClass.mjs';
const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

export const socket = io('', { query: { username } });
addAllEventListeners(socket);

socket.on('CREATE_ROOM', createRoom);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('CREATE_ROOM_ERROR', showMessage);
socket.on('JOIN_ROOM', joinRoom);
socket.on('LEAVE_ROOM_USER', leaveRoomUser);
socket.on('LEAVE_ROOM', leaveRoom);
socket.on('UPDATE_ROOM', updateRoom);
socket.on('UPDATE_ROOM_USER', updateRoomUser);
socket.on('START_TIMER', startTimer);
socket.on('CREATE_TEXT', getTextById);
socket.on('SPAWN_TEXT', spawnText);
socket.on('END_GAME', endGame);
socket.on('PROGRESS_ADD', progressAdd);
socket.on('SPAWN_GAME_TIMER', spanwGameTimer);
socket.on('GET_RESULT_TABLE', getResultTable);
socket.on('RELOAD_ROOM', reloadRoom);
//LEC 4
socket.on('CREATE_COMMENTATOR', createCommentator);
socket.on('CHANGE_PHRASE', changePhrase);
socket.on('nearly_finish_thirty', (name) => {
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('nearly_finish_thirty', { name, roomName });
});
socket.on('nearly_finish_ten', (name) => {
    const roomName = document.querySelector('.room-name').innerText;
    socket.emit('nearly_finish_ten', { name, roomName });
});
socket.on('DELETE_COMMENTATOR', deleteCommentator);
socket.on('END_GAME_COMMENTATOR', (result) => {
    const firstThree = [result[0], result[1], result[2]];
    const roomName = document.querySelector('.room-name').innerText;
    clearInterval(commentator.intervalNotTalking);
    socket.emit('END_GAME_COMMENTATOR', {
        roomName,
        firstThree,
    });
});
