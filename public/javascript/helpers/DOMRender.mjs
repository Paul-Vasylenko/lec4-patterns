import { createElement } from './helper.mjs';

export function getRoomBlock(data) {
    const roomLi = createElement({
        tagName: 'li',
        className: 'room',
    });
    const roomUsersConnected = createElement({
        tagName: 'p',
        className: 'users-connected no-select',
    });
    roomUsersConnected.innerHTML = `<span class="numOfUsers no-select ">${data.users}</span> users connected`;
    const roomNameH = createElement({
        tagName: 'h3',
        className: 'roomName no-select',
    });
    roomNameH.innerText = `${data.name}`;
    const roomJoinBtn = createElement({
        tagName: 'button',
        className: 'join-btn button',
    });
    roomJoinBtn.innerText = 'Join';

    roomLi.append(roomUsersConnected, roomNameH, roomJoinBtn);
    return roomLi;
}

export function createModal(message) {
    const modalDiv = createElement({
        tagName: 'div',
        className: 'modal',
    });
    const text = createElement({
        tagName: 'p',
        className: 'modal-text',
    });
    text.innerHTML = message;
    const okBtn = createElement({
        tagName: 'button',
        className: 'modal-closebtn',
    });
    okBtn.innerText = 'Okay';
    modalDiv.append(text, okBtn);

    return modalDiv;
}

export const showMessage = (message) => {
    const modaldiv = createModal(message);
    document.body.append(modaldiv);
    document.querySelector('.modal-closebtn').addEventListener('click', () => {
        document.querySelector('.modal').remove();
    });
    return modaldiv;
};
//currentuser, i + 1, data.statuses.get(currentuser)
export const renderPlayerBar = (currentUser, inRoom, status, roomName) => {
    if (roomName) {
        document.querySelector('.room-name').innerText = roomName;
    }
    const ul = document.querySelector('.user-list-ul');
    const li = createElement({
        tagName: 'li',
        className: 'user-list-item',
    });
    let myStatus;
    if (status == 0) {
        myStatus = createElement({
            tagName: 'div',
            className: 'ready-status-red',
        });
    } else {
        myStatus = createElement({
            tagName: 'div',
            className: 'ready-status-green',
        });
    }

    const name = createElement({
        tagName: 'div',
        className: `username ${currentUser}`,
    });
    name.innerHTML = `${currentUser} #<span class="user-number">${inRoom}</span>`;
    if (sessionStorage.getItem('username') == currentUser)
        name.innerHTML = `${currentUser} #<span class="user-number">${inRoom}</span><span class="isYou">(you)</span>`;
    const progress = createElement({
        tagName: 'div',
        className: 'progress',
    });
    const progressBar = createElement({
        tagName: 'div',
        className: `user-progress ${currentUser}`,
    });
    progress.append(progressBar);
    li.append(myStatus, name, progress);
    ul.append(li);
};

export const createTimerBlock = (maxTime) => {
    document.querySelector('#readyBtn').style.display = 'none';
    const p = createElement({
        tagName: 'span',
        className: 'timer',
        attributes: {
            id: 'timer',
        },
    });
    p.innerText = maxTime;
    const area = document.querySelector('.game-area');
    area.append(p);
};
